from django.shortcuts import render
from .models import Student
from .forms import new
from django.http import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
	if request.method == 'POST':
		newform = new(data=request.POST)
		newform.save()
		students = Student.objects.all()
		return render(request,'add_student/index.html', {'list': students, 'form':new})
	else:
		students = Student.objects.all()
		return render(request,'add_student/index.html', {'list': students, 'form':new})


def jsonview(request):
	data = serializers.serialize("xml", Student.objects.all())
	return HttpResponse(data, content_type="application/json")
