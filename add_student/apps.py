from django.apps import AppConfig


class AddStudentConfig(AppConfig):
    name = 'add_student'
