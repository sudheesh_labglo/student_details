from django import forms
from .models import Student

class new(forms.ModelForm):
	class Meta:
		model = Student
		fields = ('name', 'age')